import logging

import numpy as np
import torch

from sentiment_classification.data import load_cil_dataset, Tweets, preprocess_tweets_into_split_tweets, train_val_split
from sentiment_classification.embeddings.word2vec import embed_tweets_with_word2vec, get_or_create_word2vec_model
from sentiment_classification.folders import BUILD_FOLDER
from sentiment_classification.logger import setup_logger
from sentiment_classification.metrics import compute_binary_accuracy
from sentiment_classification.nn.fully_connected_network import FullyConnectedNetwork
from sentiment_classification.nn.simple_lstm import SimpleLSTM
from sentiment_classification.nn.cnn_lstm import CNNLSTM
from sentiment_classification.nn import create_dataloader, train

logger = logging.getLogger(__name__)

if __name__ == '__main__':

    # Hyper-parameters
    time_step = 50
    vector_size = 20
    features_per_tweet = time_step * vector_size
    epoch = 30
    batch_size = 16384
    lr = 0.02
    dropout = 0.25

    # Specific parameter for fully connected
    num_layers = 2

    # Specific parameter for LSTM-based model
    hidden_dim = 50

    # Choose one model among 'FullyConnectedNetwork', 'SimpleLSTM' and 'CNNLSTM'
    model_name = 'CNNLSTM'

    setup_logger()

    data = load_cil_dataset()
    tweets: Tweets = data[0]
    labels: np.ndarray = data[1]

    split_tweets = preprocess_tweets_into_split_tweets(tweets)

    word2vec_model = get_or_create_word2vec_model(
        str(BUILD_FOLDER / 'word2vec' / 'embedding' / f'{vector_size}.pth'),
        split_tweets,
        vector_size=vector_size,
    )

    # Tweet embedding
    X = embed_tweets_with_word2vec(
        word2vec_model,
        split_tweets,
        features_per_tweet)

    # Model identification
    if model_name == 'FullyConnectedNetwork':
        model = FullyConnectedNetwork(num_layers=num_layers, input_dim=features_per_tweet, hidden_dim=hidden_dim, dropout=dropout)
    elif model_name == 'SimpleLSTM':
        # Reshape each X sample into 2d
        X = X.reshape(-1, time_step, vector_size)
        model = SimpleLSTM(input_dim=vector_size, hidden_dim=hidden_dim, dropout=dropout)
    elif model_name == 'CNNLSTM':
        # Reshape each X sample into 2d, and then swap axes 1 and 2
        X = np.swapaxes(X.reshape(-1, time_step, vector_size), 1, 2)
        model = CNNLSTM(input_dim=vector_size, hidden_dim=hidden_dim, dropout=dropout)

    # Training-validation split
    X_train, X_val, y_train, y_val = train_val_split(X, labels, split_rate=0.8)

    # training part
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    criterion = torch.nn.BCELoss()

    dataloader_train = create_dataloader(X_train, y_train, batch_size)
    dataloader_val = create_dataloader(X_val, y_val, batch_size)

    train(
        str(BUILD_FOLDER / 'word2vec' / 'simple_lstm' / f'{vector_size}_{features_per_tweet}.pth'),
        model,
        epoch,
        dataloader_train,
        dataloader_val,
        optimizer,
        criterion)

    accuracy = compute_binary_accuracy(model(X_val), y_val)
    logger.info(f'Achieved accuracy was {accuracy}.')

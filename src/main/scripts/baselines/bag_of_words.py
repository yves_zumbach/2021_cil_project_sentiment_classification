import logging

from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, AdaBoostClassifier
from sklearn.model_selection import train_test_split

from sentiment_classification.data import load_cil_dataset
from sentiment_classification.logger import setup_logger
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer

from sentiment_classification.metrics import compute_binary_accuracy

logger = logging.getLogger(__name__)

classifier_type = 'random_forest'


if __name__ == '__main__':

    setup_logger()

    # Hyper-parameters
    word_count = 5_000

    tweets, labels = load_cil_dataset()
    X_train, X_val, y_train, y_val = train_test_split(tweets, labels, test_size=0.1, random_state=42)

    # We only keep the 5000 most frequent words, both to reduce the computational cost and reduce overfitting
    vectorizer = CountVectorizer(max_features=word_count)

    # Important: we call fit_transform on the training set, and only transform on the validation set
    logger.info(f'Vectorizing most common {word_count} words.')
    X_train = vectorizer.fit_transform(X_train)
    X_val = vectorizer.transform(X_val)

    logger.info(f'Training {classifier_type} classifier on bag-of-word embedding of tweets.')
    if classifier_type == 'logistic_regression':
        model = LogisticRegression(C=1e5, max_iter=100)
    elif classifier_type == 'random_forest':
        model = RandomForestClassifier()
    elif classifier_type == 'gradient_boosting':
        model = GradientBoostingClassifier()
    elif classifier_type == 'adaboost':
        model = AdaBoostClassifier()
    else:
        raise Exception(f'Unrecognized classifier type: {classifier_type}.')

    model.fit(X_train, y_train)
    accuracy = compute_binary_accuracy(model.predict(X_val), y_val)

    logger.info(f'Achieved accuracy was {accuracy}.')
"""
See https://github.com/EvolvedSquid/tutorials/blob/master/using-pretrained-glove-vectors/GloVe-tutorial.py.
See https://medium.com/analytics-vidhya/basics-of-using-pre-trained-glove-vectors-in-python-d38905f356db.

Download GloVe pre-trained embeddings at https://nlp.stanford.edu/projects/glove/.
"""

import logging
import xgboost as xgb

from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

from sentiment_classification.data import load_cil_dataset, preprocess_tweets_into_split_tweets
from sentiment_classification.embeddings.glove import load_glove_embedding, embed_tweets_with_glove
from sentiment_classification.folders import BUILD_FOLDER
from sentiment_classification.logger import setup_logger
from sentiment_classification.metrics import compute_binary_accuracy

logger = logging.getLogger(__name__)

classifiers = [
    xgb.XGBClassifier(),
    LogisticRegression(),
    RandomForestClassifier(),
    GradientBoostingClassifier(),
    AdaBoostClassifier(),
]
features_per_tweet = 1000


if __name__ == '__main__':

    setup_logger()

    tweets, labels = load_cil_dataset()
    split_tweets = preprocess_tweets_into_split_tweets(tweets)

    glove_embeddings = load_glove_embedding(
        str(BUILD_FOLDER / 'glove' / 'embeddings' / 'glove.twitter.27B.50d.txt')
    )
    tweet_embeddings = embed_tweets_with_glove(glove_embeddings, split_tweets, features_per_tweet)

    X_train, X_val, y_train, y_val = train_test_split(tweet_embeddings, labels, test_size=0.1, random_state=42)

    for classifier in classifiers:
        logger.info(f'Training {classifier} classifier on GloVe embedding of tweets.')
        classifier.fit(X_train, y_train)
        accuracy = compute_binary_accuracy(classifier.predict(X_val), y_val)

        logger.info(f'Accuracy on the validation set with classifier {classifier} was {accuracy}.')
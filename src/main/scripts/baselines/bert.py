#!/usr/bin/env python
# coding: utf-8

from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from tqdm import tqdm
from sentiment_classification.metrics import compute_binary_accuracy
from sentiment_classification.folders import DATA_FOLDER
from pathlib import Path

import argparse
# Parser for debugging and other
parser = argparse.ArgumentParser()
parser. add_argument("--tweets_number",
                     type=int,
                     default=200000,
                     help="number of tweets to load and embed, change for debugging reasons")

args = parser.parse_args()
tweets_number = args.tweets_number

"""
This script has referenced http://mccormickml.com/2019/05/14/BERT-word-embeddings-tutorial/.
"""
import logging
import xgboost as xgb
from typing import List

import torch
from transformers import BertTokenizer, BertModel

from sentiment_classification.data import load_cil_dataset, Tweet, Tweets
from sentiment_classification.logger import setup_logger

Token = str
"""
Bert converts words to token.
Token can either be the whole word itself if it is a common word or parts of the word or even just single letters.
"""

TokenUid = int
"""
the numerical encoding of each possible token in the BERT dataset.
"""

logger = logging.getLogger(__name__)
setup_logger(is_color=False)

classifiers = [
    xgb.XGBClassifier(n_estimators=500, max_depth=6),
    LogisticRegression(),
    RandomForestClassifier(),
    GradientBoostingClassifier(),
    AdaBoostClassifier(),
]

tweets: List[Tweet]
labels: List[int]

# Extracting all the tweets
all_tweets, all_labels = load_cil_dataset()

# Selecting the desired number of tweets
index_left = int(100000 - tweets_number/2)
index_right = int(100000 + tweets_number/2)
tweets = all_tweets[index_left:index_right]
labels = all_labels[index_left:index_right]

# Split the sentence into tokens, then retrieve he UID of each token.
logger.info('Loading BERT tokenizer.')

# Pre-trained model:
model_folder: Path = DATA_FOLDER / 'bert-base-uncased'
#  Use downloaded model
if model_folder.exists():
    logger.info('Loading BERT tokenizer from disk')
    tokenizer = BertTokenizer.from_pretrained(model_folder,
                                              local_files_only=True)  # Using local model
else:
    logger.info('Downloading BERT tokenizer from internet')
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')  # Download the model

logger.info('Encoding tweets.')
marked_tweets: List[Tweet] = ["[CLS] " + tweet + " [SEP]" for tweet in tweets]

encoded_tweets: List[List[TokenUid]] = []

# BERT was trained on pairs of sentences. In our specific use case we want to handle single sentence only data.
# BERT requires a mask to indicate to which sentence belong.
# In our case, this is simple: all words belong to sentence 1.
# We then create the list segment_id_of_tokens_in_tweets containing only "ones".
segment_id_of_tokens_in_tweets = []

for marked_tweet in tqdm(marked_tweets, position=0):
    tokenized_tweet = tokenizer.encode(marked_tweet)
    encoded_tweets.append(tokenized_tweet)
    segment_id_of_tokens_in_tweets.append([1] * len(tokenized_tweet))

# Pre-trained model:
#  Use downloaded model
if model_folder.exists():
    logger.info('Loading BERT model from disk')

    model = BertModel.from_pretrained(model_folder,
                                      local_files_only=True, output_hidden_states=True)  # Using local model
else:
    logger.info('Downloading BERT model from internet')
    model = BertModel.from_pretrained('bert-base-uncased', output_hidden_states=True)  # Download the model

# Put the model in "evaluation" mode, meaning feed-forward operation.
model.eval()

logger.info('Embedding tweets using BERT.')
sentence_embeddings = []

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = model.to(device)

with torch.no_grad():
    # Use index in order for tqdm to print a useful bar.
    for index in tqdm(range(len(encoded_tweets)), position=0):
        encoded_tweet = encoded_tweets[index]
        segment_id_of_tokens_in_tweet = segment_id_of_tokens_in_tweets[index]

        tokens_tensor = torch.tensor([encoded_tweet]).to(device)
        segments_tensors = torch.tensor([segment_id_of_tokens_in_tweet]).to(device)

        outputs = model(tokens_tensor, segments_tensors).hidden_states

        token_features = torch.cat((outputs[-1][0], outputs[-2][0], outputs[-3][0], outputs[-4][0]), dim=0)

        sentence_embedding = torch.mean(token_features, dim=0)
        sentence_embeddings.append(sentence_embedding)

# Bring back the data from the gpu to the cpu for processing and saving
tweet_embeddings = torch.stack(sentence_embeddings).to('cpu').numpy()

X_train, X_val, y_train, y_val = train_test_split(tweet_embeddings, labels, test_size=0.2, random_state=42)

for classifier in classifiers:
    logger.info(f'Training {classifier} classifier on Bert embedding of tweets.')
    classifier.fit(X_train, y_train)
    accuracy = compute_binary_accuracy(classifier.predict(X_val), y_val)

    logger.info(f'Accuracy on the validation set with classifier {classifier} was {accuracy}.')

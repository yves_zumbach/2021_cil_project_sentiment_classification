import logging
from datetime import datetime
from os import makedirs
from pathlib import Path

import sklearn.linear_model
import xgboost
from joblib import dump, load
import numpy as np
import pandas as pd
from sklearn.ensemble import VotingClassifier, RandomForestClassifier
from sklearn.linear_model import LogisticRegression

from sentiment_classification.folders import DATA_FOLDER, BUILD_FOLDER
from sentiment_classification.logger import setup_logger

data_folder: Path = DATA_FOLDER / 'bertweet'
model_folder: Path = BUILD_FOLDER / 'bertweet'
train_tweets_embedding_path = 'bertweet_embedding_full.npy'
train_tweets_label_path = 'bertweet_embedding_full_label.npy'
test_tweets_embedding_path = 'bertweet_embedding_test.npy'

features_per_tweet = 768

# The classifiers to use in the voting classifier
voting_classifiers = [
    ('logistic_regression', LogisticRegression(max_iter=1000, verbose=1)),
]


def train_and_save_classifier(
        classifier,
        name: str,
        train_embeddings,
        train_labels) -> None:
    # Train classifier
    classifier.fit(train_embeddings, train_labels)

    # Save model to disk
    makedirs(BUILD_FOLDER / 'bertweet_ensemble_classifier', exist_ok=True)
    dump(classifier, BUILD_FOLDER / name)


if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    setup_logger(is_color=True)

    logger.info(f'Training Logistic Regression classifier on the full data set')

    # Load tweet embeddings
    logger.info(f'Loading data from disk.')
    train_embeddings = np.load(data_folder / train_tweets_embedding_path)
    train_labels = np.load(data_folder / train_tweets_label_path)
    logger.info(f'loaded')

    # Train voting classifier
    now = datetime.now()
    name = now.strftime('Logistic_Regression_classifier_%Y-%m-%d-%H-%M-%S.joblib')
    voting_classifier = VotingClassifier(
        estimators=voting_classifiers,
        voting='hard'
    )
    train_and_save_classifier(voting_classifier, name, train_embeddings, train_labels)

    # Make predictions and save them to disk
    test_embeddings = np.load(data_folder / test_tweets_embedding_path)
    predictions_y = voting_classifier.predict(test_embeddings)
    predictions_y[predictions_y < 0.5] = 0
    predictions_y[predictions_y >= 0.5] = 1

    result = pd.DataFrame()
    result['Id'] = np.arange(0, len(test_embeddings))
    result['Prediction'] = predictions_y
    result.to_csv(
        BUILD_FOLDER / 'bertweet_ensemble_classifier' / now.strftime('predictions_%Y-%m-%d-%H-%M-%S.csv'),
        index=False
    )

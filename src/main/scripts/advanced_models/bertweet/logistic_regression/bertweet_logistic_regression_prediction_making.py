import logging
from datetime import datetime
from os import makedirs

from joblib import load
import numpy as np
import pandas as pd

from sentiment_classification.folders import BUILD_FOLDER, DATA_FOLDER
from sentiment_classification.logger import setup_logger

classifier_filename = 'ensemble_classifier_2021-07-21-22-00-53.joblib'

if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    setup_logger(is_color=True)
    now = datetime.now()

    logger.info('Making predictions for test dataset using the trained bertweet logistic regression classifier.')

    logger.info('Loading bertweet embeddings of test dataset.')
    test_embeddings = np.load(DATA_FOLDER / 'bertweet' / 'bertweet_embedding_test.npy')

    logger.info('Reading the classfier model from disk.')
    # We read the ensemble classifier model from disk,
    # then change the weights of each model to only keep
    # the outputs of the logistic regression model in the ensemble model.
    classifier = load(BUILD_FOLDER / 'bertweet_ensemble_classifier' / classifier_filename)
    classifier.weights = [1, 0, 0]

    logger.info('Running test samples through the logistic regression classifier model.')
    predictions_y = classifier.predict(test_embeddings)
    predictions_y[predictions_y == 1] = -1
    predictions_y[predictions_y == 0] = 1

    result = pd.DataFrame()
    result['Id'] = np.arange(1, len(test_embeddings) + 1)
    result['Prediction'] = predictions_y
    makedirs(BUILD_FOLDER / 'bertweet_logistic_regression_classifier', exist_ok=True)
    result.to_csv(
        BUILD_FOLDER / 'bertweet_logistic_regression_classifier' / now.strftime('bertweet_logistic_regression_predictions_%Y-%m-%d-%H-%M-%S.csv'),
        index=False
    )

from pathlib import Path

import numpy as np
import pandas as pd
from tqdm import tqdm

from sentiment_classification.folders import DATA_FOLDER

import logging
from typing import List

import torch
from transformers import AutoTokenizer, AutoModelForSequenceClassification
from sentiment_classification.data import load_cil_test_dataset, Tweet
from sentiment_classification.logger import setup_logger

Token = str
"""
Bert converts words to token.
Token can either be the whole word itself if it is a common word or parts of the word or even just single letters.
"""

TokenUid = int
"""
the numerical encoding of each possible token in the BERT dataset.
"""

logger = logging.getLogger(__name__)
setup_logger(is_color=False)

# Max length for the sentences
MAX_LENGTH = 50

if __name__ == '__main__':

    tweets: List[Tweet]
    labels: List[int]

    tweets, _ = load_cil_test_dataset()

    tweets = tweets
    # Fine-tuned model:
    model_folder: Path = DATA_FOLDER / 'bertweet-fine-tuned-len-40'

    logger.info('Loading BERTweet tokenizer from disk')
    tokenizer = AutoTokenizer.from_pretrained(model_folder,
                                              local_files_only=True)  # Using local model
    model = AutoModelForSequenceClassification.from_pretrained(model_folder, num_labels=2, output_attentions=False,
                                                               output_hidden_states=False)

    input_ids = []
    attention_masks = []

    for tweet in tqdm(tweets, position=0):
        encoded_dict = tokenizer.encode_plus(
            tweet,
            padding="max_length",
            max_length=MAX_LENGTH,
            truncation=True,
            add_special_tokens=True,
            return_attention_mask=True,
            return_tensors='pt'
        )

        input_ids.append(encoded_dict['input_ids'])
        attention_masks.append(encoded_dict['attention_mask'])

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    input_ids = torch.cat(input_ids, dim=0).to(device)
    attention_masks = torch.cat(attention_masks, dim=0).to(device)
    model = model.to(device)

    logits = []

    with torch.no_grad():
        for i in tqdm(range(100), position=0):
            outputs = model(input_ids[i*100: (i+1)*100],
                            token_type_ids=None,
                            attention_mask=attention_masks[i*100: (i+1)*100])
            logit = outputs.logits
            logits.append(logit)

    logits = torch.cat(logits, dim=0).detach().cpu().numpy()

    preds = np.argmax(logits, axis=1).flatten()
    for i in range(len(preds)):
        if preds[i] == 0:
            preds[i] = -1

    result = pd.DataFrame()
    result['Id'] = np.arange(1, 10001)
    result['Prediction'] = preds.astype(int)
    result.to_csv('test_trainer/bertweet_fine_tuned_40.csv', index=False)
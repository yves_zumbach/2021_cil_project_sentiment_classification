from pathlib import Path

from tqdm import tqdm

import logging
import os
import random
import numpy as np
from typing import List

import torch
from torch.utils.data import TensorDataset, random_split, DataLoader, RandomSampler, SequentialSampler

from transformers import AutoTokenizer, AutoModelForSequenceClassification, get_linear_schedule_with_warmup, AdamW
from sentiment_classification.data import load_cil_dataset, Tweet
from sentiment_classification.logger import setup_logger
from sentiment_classification.folders import DATA_FOLDER

'''
This fine-tuning script has made some reference to 
    https://mccormickml.com/2019/07/22/BERT-fine-tuning/#advantages-of-fine-tuning
It fine tunes the pretrained BERTweet model, and adapt the weights of the per-trained BERTweet model to our sentiment classification task.
'''

logger = logging.getLogger(__name__)
setup_logger(is_color=False)

# Max length for the sentences
MAX_LENGTH = 50

# Batch size used during fine-tuning
BATCH_SIZE = 128

# Train-val split rate
SPLIT_RATE = 0.9

# Learning rate
LEARNING_RATE = 1e-5

# Epoch
EPOCH = 2

# Random seed
SEED = 42


# Helper function to calculate accuracy
def calc_accuracy(preds, labels):
    preds_flat = np.argmax(preds, axis=1).flatten()
    labels_flat = labels.flatten()
    return np.sum(preds_flat == labels_flat) / len(labels_flat)

if __name__ == '__main__':

    tweets: List[Tweet]
    labels: List[int]

    # Extract all the tweets and labels in the partial dataset
    tweets, labels = load_cil_dataset(load_partial_data=True)

    # Model load and output directories
    model_folder: Path = DATA_FOLDER / 'bertweet'
    model_output_dir = DATA_FOLDER / 'bertweet-fine-tuned'

    # Load model and tokenizer
    if model_folder.exists():
        logger.info('Loading BERTweet tokenizer and classification model from disk...')
        tokenizer = AutoTokenizer.from_pretrained(model_folder, local_files_only=True)
        classification_model = AutoModelForSequenceClassification.from_pretrained(model_folder, num_labels=2, output_attentions=False, output_hidden_states=False)
    else:
        logger.info('Downloading BERTweet tokenizer from internet...')
        tokenizer = AutoTokenizer.from_pretrained("vinai/bertweet-base", use_fast=False)
        classification_model = AutoModelForSequenceClassification.from_pretrained("vinai/bertweet-base", num_labels=2, output_attentions=False, output_hidden_states=False)

    logger.info('Encoding tweets.')
    input_ids = []
    attention_masks = []

    for tweet in tqdm(tweets, position=0):
        tokenized_dict = tokenizer.encode_plus(
            tweet,
            padding='max_length',
            max_length=MAX_LENGTH,
            truncation=True,
            add_special_tokens=True,
            return_attention_mask=True,
            return_tensors='pt'
        )

        input_ids.append(tokenized_dict['input_ids'])
        attention_masks.append(tokenized_dict['attention_mask'])

    input_ids = torch.cat(input_ids, dim=0)
    attention_masks = torch.cat(attention_masks, dim=0)
    labels = torch.tensor(labels)
    dataset = TensorDataset(input_ids, attention_masks, labels)

    # Train-val split
    train_size = int(len(dataset) * SPLIT_RATE)
    val_size = len(dataset) - train_size
    train_dataset, val_dataset = random_split(dataset, [train_size, val_size])


    dataloader_train = DataLoader(
        train_dataset,
        batch_size=BATCH_SIZE,
        sampler=RandomSampler(train_dataset)
    )

    dataloader_val = DataLoader(
        val_dataset,
        batch_size=BATCH_SIZE,
        sampler=SequentialSampler(val_dataset)
    )

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    classification_model = classification_model.to(device)

    optimizer = AdamW(classification_model.parameters(), lr=LEARNING_RATE)


    total_steps = len(dataloader_train) * EPOCH
    scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=total_steps)

    random.seed(SEED)
    np.random.seed(SEED)
    torch.manual_seed(SEED)
    torch.cuda.manual_seed_all(SEED)

    # Training
    for epoch_i in range(0, EPOCH):

        logger.info('Epoch {}/{} started!'.format(epoch_i + 1, EPOCH))

        # Reset the total loss for this epoch.
        total_train_loss = 0
        classification_model.train()

        # For each batch of training data...
        for step, batch in enumerate(dataloader_train):

            if step % 50 == 0 and not step == 0:
                # Report progress.
                logger.info('Batch {}/{}: '.format(step, len(dataloader_train)))
                logger.info('Loss: {}'.format(loss.item()))

            # Get corresponding data from the batch
            batch_input_ids = batch[0].to(device)
            batch_input_mask = batch[1].to(device)
            batch_labels = batch[2].to(device)

            classification_model.zero_grad()
            outputs = classification_model(batch_input_ids,
                                           token_type_ids=None,
                                           attention_mask=batch_input_mask,
                                           labels=batch_labels)
            loss, logits = outputs.loss, outputs.logits

            total_train_loss += loss.item()

            loss.backward()
            torch.nn.utils.clip_grad_norm_(classification_model.parameters(), 1.0)
            optimizer.step()
            scheduler.step()

        # Whole batch loss
        avg_train_loss = total_train_loss / len(dataloader_train)

        logger.info("Average training loss: {}".format(avg_train_loss))

        # Validation phase
        logger.info("Validation in progress...")

        classification_model.eval()
        total_val_accuracy = 0
        total_val_loss = 0
        nb_eval_steps = 0

        for batch in dataloader_val:
            batch_input_ids = batch[0].to(device)
            batch_input_mask = batch[1].to(device)
            batch_labels = batch[2].to(device)

            with torch.no_grad():
                outputs = classification_model(batch_input_ids,
                                               token_type_ids=None,
                                               attention_mask=batch_input_mask,
                                               labels=batch_labels)
                loss, logits = outputs.loss, outputs.logits

            total_val_loss += loss.item()
            logits = logits.detach().cpu().numpy()
            label_ids = batch_labels.to('cpu').numpy()
            total_val_accuracy += calc_accuracy(logits, label_ids)

        val_accuracy = total_val_accuracy / len(dataloader_val)
        val_loss = total_val_loss / len(dataloader_val)

        logger.info("Validation accuracy: {}, corresponding loss: {}".format(val_accuracy, val_loss))

    logger.info("Training complete!")
    logger.info("Saving model...")
    classification_model = classification_model.to(torch.device('cpu'))


    if not os.path.exists(model_output_dir):
        os.makedirs(model_output_dir)

    logger.info("Saving model to {}...".format(model_output_dir))
    model_to_save = classification_model.module if hasattr(classification_model, 'module') else classification_model
    model_to_save.save_pretrained(model_output_dir)
    tokenizer.save_pretrained(model_output_dir)
    logger.info("Done!")

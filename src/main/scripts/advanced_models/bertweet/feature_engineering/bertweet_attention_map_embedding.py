import argparse
import os
from pathlib import Path

import numpy as np
from tqdm import tqdm

from sentiment_classification.folders import DATA_FOLDER

# Parser for debugging and other
parser = argparse.ArgumentParser()
parser.add_argument("--left_index",
                    type=int,
                    default=0,
                    help="left index of the tweets to be loaded")
parser.add_argument("--right_index",
                    type=int,
                    default=2,
                    help="right index of the tweets to be loaded")
parser.add_argument("--test_set",
                    type=int,
                    default=0,
                    help="0 for training dataset, 1 for testing dataset")

args = parser.parse_args()
left_index = args.left_index
right_index = args.right_index
is_test_set = args.test_set

"""
This script has referenced: http://mccormickml.com/2019/05/14/BERT-word-embeddings-tutorial/.
"""
import logging
from typing import List

import torch
from transformers import AutoModel, AutoTokenizer, AutoModelForSequenceClassification

from sentiment_classification.data import load_cil_dataset, load_cil_test_dataset, Tweet
from sentiment_classification.logger import setup_logger


Token = str
"""
Bert converts words to token.
Token can either be the whole word itself if it is a common word or parts of the word or even just single letters.
"""

TokenUid = int
"""
the numerical encoding of each possible token in the BERT dataset.
"""


logger = logging.getLogger(__name__)
setup_logger(is_color=False)

# There is one abnormal tweet that cannot be encoded
ABNORMAL_INDEX = 212494

# Regularized attention map size:
K = 16


def regularize_attention_map(attention_map, K, device):
    '''
    Regularize all attention maps into the same size by convolution operation
    Args:
        attention_map(torch.Tensor(N * N))
        K(int): size of the regularized attention map (K * K)
    Returns:
        regularized_map(torch.Tensor(K * K))
    '''
    N = attention_map.size()[0]

    if N == K:
        return attention_map
    regularized_map = torch.zeros(K, K).to(device)
    if N < K:
        for i in range(K):
            for j in range(K):
                regularized_map[i][j] = attention_map[i % N][j % N]
    else:
        for i in range(K):
            for j in range(K):
                if i < K // 2 and j < K // 2:
                    regularized_map[i][j] = attention_map[i][j]
                elif i < K // 2 and j >= K // 2:
                    regularized_map[i][j] = attention_map[i][-(K - j)]
                elif i > K // 2 and j < K // 2:
                    regularized_map[i][j] = attention_map[-(K - i)][j]
                else:
                    regularized_map[i][j] = attention_map[-(K - i)][-(K - j)]
    return regularized_map


if __name__ == '__main__':

    tweets: List[Tweet]
    labels: List[int]

    # Extracting all the tweets
    if is_test_set:
        all_tweets, all_labels = load_cil_test_dataset()
    else:
        all_tweets, all_labels = load_cil_dataset(load_partial_data=False)
        all_tweets = np.delete(all_tweets, ABNORMAL_INDEX)
        all_labels = np.delete(all_labels, ABNORMAL_INDEX)

    # Selecting the desired number of tweets
    tweets = all_tweets[left_index:right_index] if is_test_set == 0 else all_tweets
    labels = all_labels[left_index:right_index] if is_test_set == 0 else None

    # Split the sentence into tokens, then retrieve he UID of each token.
    logger.info('Loading BERT tokenizer.')

    # Pre-trained model:
    model_folder: Path = DATA_FOLDER / 'bertweet-fine-tuned'
    #  Use downloaded model
    if model_folder.exists():
        logger.info('Loading BERTweet tokenizer from disk')
        tokenizer = AutoTokenizer.from_pretrained(model_folder,
                                                  local_files_only=True)  # Using local model
        classification_model = AutoModelForSequenceClassification.from_pretrained(model_folder, local_files_only=True,
                                                                                  output_attentions=True,
                                                                                  output_hidden_states=True)
    else:
        logger.info('Downloading BERTweet tokenizer from internet')
        tokenizer = AutoTokenizer.from_pretrained("vinai/bertweet-base", use_fast=False)
        classification_model = AutoModelForSequenceClassification.from_pretrained("vinai/bertweet-base", use_fast=False,
                                                                                  output_attentions=True,
                                                                                  output_hidden_states=True)

    logger.info('Encoding tweets.')
    marked_tweets: List[Tweet] = ["[CLS] " + tweet + " [SEP]" for tweet in tweets]

    encoded_tweets: List[List[TokenUid]] = []

    segment_id_of_tokens_in_tweets = []

    for marked_tweet in tqdm(marked_tweets, position=0):
        tokenized_tweet = tokenizer.encode(marked_tweet)
        encoded_tweets.append(tokenized_tweet)
        segment_id_of_tokens_in_tweets.append([1] * len(tokenized_tweet))

    # Put the model in "evaluation" mode, meaning feed-forward operation.
    classification_model.eval()

    logger.info('Embedding tweets using BERTweet.')
    sentence_embeddings = []

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    classification_model = classification_model.to(device)

    with torch.no_grad():
        # Use index in order for tqdm to print a useful bar.
        iter_range = right_index - left_index if is_test_set == 0 else len(tweets)

        for index in tqdm(range(iter_range), position=0):
            encoded_tweet = encoded_tweets[index]
            segment_id_of_tokens_in_tweet = segment_id_of_tokens_in_tweets[index]

            tokens_tensor = torch.tensor([encoded_tweet]).to(device)
            segments_tensors = torch.tensor([segment_id_of_tokens_in_tweet]).to(device)

            attentions = classification_model(tokens_tensor, segments_tensors).attentions[0][0]
            attention_map = torch.mean(attentions[-4:], dim=0)
            regularized_attention_map = regularize_attention_map(attention_map, K, device)
            sentence_embeddings.append(regularized_attention_map)

            del tokens_tensor
            del segments_tensors
            del attention_map
            del regularized_attention_map

    # Bring back the data from the gpu to the cpu for processing and saving
    tweet_embeddings = torch.stack(sentence_embeddings).detach().cpu().numpy()

    # Save the embeddings for later user

    if is_test_set:
        np.save(os.path.join(model_folder, 'bertweet_embedding_attention_map_test.npy'.format(left_index, right_index)),
                tweet_embeddings)
    else:
        np.save(
            os.path.join(model_folder, 'bertweet_embedding_attention_map_l={}_r={}.npy'.format(left_index, right_index)),
            tweet_embeddings)
        np.save(os.path.join(model_folder, 'bertweet_embedding_attention_map_l={}_r={}_label.npy').format(left_index, right_index), labels)

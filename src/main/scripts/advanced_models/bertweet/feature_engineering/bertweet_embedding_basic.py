import argparse
import os
from pathlib import Path

import numpy as np
from tqdm import tqdm

from sentiment_classification.folders import DATA_FOLDER

# Parser for debugging and other
parser = argparse.ArgumentParser()
parser.add_argument("--left_index",
                    type=int,
                    default=0,
                    help="left index of the tweets to be loaded")
parser.add_argument("--right_index",
                    type=int,
                    default=2499999,
                    help="right index of the tweets to be loaded")
parser.add_argument("--test_set",
                    type=int,
                    default=0,
                    help="0 for training dataset, 1 for testing dataset")

args = parser.parse_args()
left_index = args.left_index
right_index = args.right_index
is_test_set = args.test_set

"""
This script has referenced http://mccormickml.com/2019/05/14/BERT-word-embeddings-tutorial/.
"""
import logging
from typing import List

import torch
from transformers import AutoModel, AutoTokenizer

from sentiment_classification.data import load_cil_dataset, load_cil_test_dataset, Tweet
from sentiment_classification.logger import setup_logger

Token = str
"""
Bert converts words to token.
Token can either be the whole word itself if it is a common word or parts of the word or even just single letters.
"""

TokenUid = int
"""
the numerical encoding of each possible token in the BERT dataset.
"""


logger = logging.getLogger(__name__)
setup_logger(is_color=False)

# There is one abnormal tweet that cannot be encoded
ABNORMAL_INDEX = 212494

if __name__ == '__main__':

    tweets: List[Tweet]
    labels: List[int]

    # Extracting all the tweets
    if is_test_set:
        all_tweets, all_labels = load_cil_test_dataset()
    else:
        all_tweets, all_labels = load_cil_dataset(load_partial_data=False)
        all_tweets = np.delete(all_tweets, ABNORMAL_INDEX)
        all_labels = np.delete(all_labels, ABNORMAL_INDEX)

    # Selecting the desired number of tweets
    tweets = all_tweets[left_index:right_index] if is_test_set == 0 else all_tweets
    labels = all_labels[left_index:right_index] if is_test_set == 0 else None

    # Split the sentence into tokens, then retrieve he UID of each token.
    logger.info('Loading BERT tokenizer.')

    # Pre-trained model:
    model_folder: Path = DATA_FOLDER / 'bertweet-fine-tuned'
    #  Use downloaded model
    if model_folder.exists():
        logger.info('Loading BERTweet tokenizer from disk')
        tokenizer = AutoTokenizer.from_pretrained(model_folder,
                                                  local_files_only=True)  # Using local model
    else:
        logger.info('Downloading BERTweet tokenizer from internet')
        tokenizer = AutoTokenizer.from_pretrained("vinai/bertweet-base", use_fast=False)

    logger.info('Encoding tweets.')
    marked_tweets: List[Tweet] = ["[CLS] " + tweet + " [SEP]" for tweet in tweets]

    encoded_tweets: List[List[TokenUid]] = []
    segment_id_of_tokens_in_tweets = []

    for marked_tweet in tqdm(marked_tweets, position=0):
        tokenized_tweet = tokenizer.encode(marked_tweet)
        encoded_tweets.append(tokenized_tweet)
        segment_id_of_tokens_in_tweets.append([1] * len(tokenized_tweet))

    # Pre-trained model:
    if model_folder.exists():
        logger.info('Loading BERTweet model from disk')

        model = AutoModel.from_pretrained(model_folder,
                                          local_files_only=True, output_hidden_states=True)  # Using local model
    else:
        logger.info('Downloading BERTweet model from internet')
        model = AutoModel.from_pretrained("vinai/bertweet-base", output_hidden_states=True)

    if not model_folder.exists():
        os.mkdir(model_folder)
        tokenizer.save_pretrained(model_folder)
        model.save_pretrained(model_folder)

    model.eval()

    logger.info('Embedding tweets using BERTweet.')
    sentence_embeddings = []

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = model.to(device)

    with torch.no_grad():
        # Use index in order for tqdm to print a useful bar.
        iter_range = right_index - left_index if is_test_set == 0 else len(tweets)

        for index in tqdm(range(iter_range), position=0):
            encoded_tweet = encoded_tweets[index]
            segment_id_of_tokens_in_tweet = segment_id_of_tokens_in_tweets[index]

            tokens_tensor = torch.tensor([encoded_tweet]).to(device)
            segments_tensors = torch.tensor([segment_id_of_tokens_in_tweet]).to(device)

            outputs = model(tokens_tensor, segments_tensors).hidden_states

            token_features = torch.cat((outputs[-1][0], outputs[-2][0], outputs[-3][0], outputs[-4][0]), dim=0)

            print(token_features.size())
            sentence_embedding = torch.mean(token_features, dim=0).detach().cpu()
            sentence_embeddings.append(sentence_embedding)

            del tokens_tensor
            del segments_tensors
            del outputs
            del token_features

    # Bring back the data from the gpu to the cpu for processing and saving
    tweet_embeddings = torch.stack(sentence_embeddings)

    # Save the embeddings for later user

    if is_test_set:
        np.save(os.path.join(model_folder, 'bertweet_embedding_test.npy'.format(left_index, right_index)),
                tweet_embeddings)
    else:
        np.save(os.path.join(model_folder, 'bertweet_embedding_l={}_r={}.npy'.format(left_index, right_index)),
                tweet_embeddings)
        np.save(os.path.join(model_folder, 'bertweet_embedding_l={}_r={}_label.npy').format(left_index, right_index), labels)
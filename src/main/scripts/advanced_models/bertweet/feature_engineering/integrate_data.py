import numpy as np
from sentiment_classification.folders import DATA_FOLDER

DATA_FOLDER = DATA_FOLDER / 'bertweet-fine-tuned'
embedding_list = [
    DATA_FOLDER / 'bertweet_embedding_l=0_r=500000.npy',
    DATA_FOLDER / 'bertweet_embedding_l=500000_r=1000000.npy',
    DATA_FOLDER / 'bertweet_embedding_l=1000000_r=1500000.npy',
    DATA_FOLDER / 'bertweet_embedding_l=1500000_r=2000000.npy',
    DATA_FOLDER / 'bertweet_embedding_l=2000000_r=2499999.npy'
]

attention_map_list = [
    DATA_FOLDER / 'bertweet_embedding_attention_map_l=0_r=500000.npy',
    DATA_FOLDER / 'bertweet_embedding_attention_map_l=500000_r=1000000.npy',
    DATA_FOLDER / 'bertweet_embedding_attention_map_l=1000000_r=1500000.npy',
    DATA_FOLDER / 'bertweet_embedding_attention_map_l=1500000_r=2000000.npy',
    DATA_FOLDER / 'bertweet_embedding_attention_map_l=2000000_r=2499999.npy'
]

label_list = [
    DATA_FOLDER / 'bertweet_embedding_l=0_r=500000_label.npy',
    DATA_FOLDER / 'bertweet_embedding_l=500000_r=1000000_label.npy',
    DATA_FOLDER / 'bertweet_embedding_l=1000000_r=1500000_label.npy',
    DATA_FOLDER / 'bertweet_embedding_l=1500000_r=2000000_label.npy',
    DATA_FOLDER / 'bertweet_embedding_l=2000000_r=2499999_label.npy'
]

embedding_test_path = DATA_FOLDER / 'bertweet_embedding_test.npy'
attention_map_test_path = DATA_FOLDER / 'bertweet_embedding_attention_map_test.npy'

if __name__ == '__main__':
    embedding = np.concatenate([np.load(e) for e in embedding_list], axis=0)
    label = np.concatenate([np.load(l) for l in label_list], axis=0)
    attention_map = np.concatenate([np.load(a) for a in attention_map_list], axis=0)

    indices = np.random.permutation(len(label))

    folds = []
    folds.append(indices[:500000])
    folds.append(indices[500000:1000000])
    folds.append(indices[1000000:1500000])
    folds.append(indices[1500000:2000000])
    folds.append(indices[2000000:2499999])

    for i in range(len(folds)):
        fold = folds[i]
        print(fold)
        print(fold.shape)
        data = embedding[fold]
        attention = attention_map[fold].reshape(-1, 256)

        data = np.concatenate((data, attention), axis=1)
        data_label = label[fold]
        print(data.shape)
        print(data_label.shape)
        np.save(DATA_FOLDER / 'bertweet_embedding_fold_{}.npy'.format(i+1), data)
        np.save(DATA_FOLDER / 'bertweet_embedding_fold_{}_label.npy'.format(i+1), data_label)

    test_embedding = np.load(embedding_test_path)
    test_attention = np.load(attention_map_test_path).reshape(-1, 256)
    test_data = np.concatenate((data, attention), aixs=1)
    np.save(DATA_FOLDER / 'bertweet_embedding_test.npy', test_data)



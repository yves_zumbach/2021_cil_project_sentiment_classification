import logging
from datetime import datetime
from os import makedirs

from joblib import load
import numpy as np
import pandas as pd

from sentiment_classification.folders import BUILD_FOLDER, DATA_FOLDER
from sentiment_classification.logger import setup_logger

classifier_filename = 'ensemble_classifier_2021-07-21-22-00-53.joblib'

if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    setup_logger(is_color=True)
    now = datetime.now()

    logger.info('Making predictions for test dataset using the trained bertweet ensemble classifier.')

    logger.info('Loading bertweet embeddings of test dataset.')
    test_embeddings = np.load(DATA_FOLDER / 'bertweet' / 'bertweet_embedding_test.npy')

    logger.info('Reading the ensemble classfier model from disk.')
    classifier = load(BUILD_FOLDER / 'bertweet_ensemble_classifier' / classifier_filename)

    logger.info('Running test samples through ensemble classifier model.')
    predictions_y = classifier.predict(test_embeddings)
    predictions_y[predictions_y == 1] = -1
    predictions_y[predictions_y == 0] = 1

    result = pd.DataFrame()
    result['Id'] = np.arange(1, len(test_embeddings) + 1)
    result['Prediction'] = predictions_y
    makedirs(BUILD_FOLDER / 'bertweet_ensemble_classifier', exist_ok=True)
    result.to_csv(
        BUILD_FOLDER / 'bertweet_ensemble_classifier' / now.strftime('bertweet_ensemble_classifier_predictions_%Y-%m-%d-%H-%M-%S.csv'),
        index=False
    )

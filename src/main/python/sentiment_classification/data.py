import logging
from typing import List, Tuple

import gensim
import numpy as np
import torch

from sentiment_classification.folders import CIL_DATA_FOLDER

logger = logging.getLogger(__name__)

Tweet = str
"""
A single tweet encoded as a single string where words are separated with a space.
"""

Tweets = List[Tweet]
"""
A list of string where each string is a single tweet.
"""

SplitTweet = List[str]
"""
A single tweet encoded as a list of str where each string is a word from the tweet.
"""

SplitTweets = List[SplitTweet]
"""
A list of list of string, where a single tweet is represented as a list of word and many tweets are aggregated into
a single list.
"""

Word = str
"""
A single word encoded as a string.
"""


def load_tweets(filename: str, label: int, tweets: List[str], labels: List[int]) -> None:
    """
    Loads all the tweets from the given file to the provided `tweets` list. For each entry added to the `tweets` list,
    a label with value `label` will be appended to the provided `labels` list.
    :param filename: The name of the file containing tweets. There should be one tweet per line.
    :param label: The label corresponding to the tweets being loaded (positive or negative tweet).
    :param tweets: The array in which to store the tweets.
    :param labels: The array in which to store the label corresponding to the loaded tweets.
    """
    with open(filename, 'r', encoding='utf-8') as f:
        for line in f:
            tweets.append(line.rstrip())
            labels.append(label)


def load_cil_dataset(load_partial_data: bool = True) -> Tuple[Tweets, np.array]:
    """
    Loads the CIL dataset from disk.
    :param load_partial_data: Whether to load the full dataset of just a small part of it.
    :return: Two things. First a python list containing one string per tweet, i.e. the words are not parsed.
    You will generally want to process this list further to convert it to a shape that can be used in ML algorithms.
    Second, a numpy array containing the label, i.e. 0 or 1, for every tweet.
    """
    if load_partial_data:
        dataset_path_pos = CIL_DATA_FOLDER / 'train_pos.txt'
        dataset_path_neg = CIL_DATA_FOLDER / 'train_neg.txt'
    else:
        dataset_path_pos = CIL_DATA_FOLDER / 'train_neg_full.txt'
        dataset_path_neg = CIL_DATA_FOLDER / 'train_pos_full.txt'

    tweets: List[str] = []
    labels: List[int] = []

    load_tweets(dataset_path_neg, 0, tweets, labels)
    load_tweets(dataset_path_pos, 1, tweets, labels)

    logger.info(f'{len(tweets)} tweets loaded.')
    return tweets, np.array(labels)


def load_cil_test_dataset() -> Tuple[Tweets, None]:
    """
    Loads the CIL test dataset from disk.
    :return: Two things. First a python list containing one string per tweet, i.e. the words are not parsed.
    You will generally want to process this list further to convert it to a shape that can be used in ML algorithms.
    Second, a numpy array containing the label, i.e. 0 or 1, for every tweet.
    """

    dataset_path_pos = CIL_DATA_FOLDER / 'test_data.txt'

    tweets: List[str] = []

    with open(dataset_path_pos, 'r', encoding='utf-8') as f:
        for line in f:
            tweets.append(line.rstrip())

    logger.info(f'{len(tweets)} tweets loaded.')
    return tweets, None


def preprocess_tweets_into_split_tweets(tweets: Tweets) -> SplitTweets:
    """
    Preprocesses tweets into split tweets. The tweets are tokenized, de-accentuated, brought to lower case and only
    words of medium length are kept, i.e. short words and very long ones are discarded.
    :param tweets: The tweets to preprocess.
    :return: The preprocessed tweets as split tweets.
    """
    split_tweets = []
    for tweet in tweets:
        split_tweets.append(gensim.utils.simple_preprocess(tweet, deacc=True))
    return split_tweets


def train_val_split(x: np.array, y: np.array, split_rate: float = 0.8):
    """
    Takes some data and their corresponding labels, then split them in a training set and a validation set.
    :param x: The data to split.
    :param y: The label of the data to split.
    :param split_rate: The ratio of training VS validation data.
    :return: 4 PyTorch arrays: the training data, the validation data, the training labels and the validation labels.
    """
    shuffled_indices = np.random.permutation(len(y))
    split_idx = int(split_rate * len(y))
    train_indices = shuffled_indices[:split_idx]
    val_indices = shuffled_indices[split_idx:]

    x_train = x[train_indices]
    x_val = x[val_indices]
    y_train = y[train_indices]
    y_val = y[val_indices]

    x_train = torch.from_numpy(x_train).float()
    x_val = torch.from_numpy(x_val).float()
    y_train = torch.from_numpy(y_train.reshape(-1, 1)).float()
    y_val = torch.from_numpy(y_val.reshape(-1, 1)).float()

    return x_train, x_val, y_train, y_val

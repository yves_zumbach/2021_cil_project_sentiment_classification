"""
Logger setup for the project.
"""

import sys

from colorlog import ColoredFormatter
import logging


def setup_logger(log_level=logging.INFO, is_color: bool = True):
    """
    Setups the root logger.
    :param log_level: The level of logs that should be printed.
    :param is_color: Whether output logs should be colored.
    """
    # Root Logger
    logger = logging.getLogger()
    logger.setLevel(log_level)

    # Remove old handlers if any (useful for jupyter notebooks for example)
    for handler in logger.handlers:
        logger.removeHandler(handler)

    # Print logs to the console
    console_handler = logging.StreamHandler(sys.stdout)
    logger.addHandler(console_handler)

    colored_format = '{asctime} {log_color}[{levelname}] {message}{reset} {blue}in {pathname}:{lineno}' \
        if is_color \
        else '{asctime} [{levelname}] {message} in {pathname}:{lineno}'

    # Pretty print if platform supports it
    console_handler.setFormatter(ColoredFormatter(
        fmt=colored_format,
        log_colors={
            'DEBUG': 'white',
            'INFO': 'bold',
            'WARNING': 'bold_yellow',
            'ERROR': 'red',
            'CRITICAL': 'bold_red',
        },
        style='{'
    ))

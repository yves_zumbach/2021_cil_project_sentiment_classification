import logging
import numpy as np
import os
from typing import Union, List

import gensim
from gensim.models import Doc2Vec

from sentiment_classification.data import Tweets, SplitTweet


logger = logging.getLogger(__name__)


def generate_word2vec_corpus(
        tweets: Tweets,
        is_validation: bool
) -> List[Union[SplitTweet, gensim.models.doc2vec.TaggedDocument]]:
    """
    Generates a corpus for the word2vec embedding model.
    :param tweets: The tweets to preprocess.
    :param is_validation: Whether the current data is the validation set or not.
    :return: A generator yielding the parsed tweets. If the current tweets are training tweets,
    then TaggedDocuments will be returned.
    If the current tweets are validation tweets, then only SplitTweets will be returned.
    """
    output = []
    for index, tweet in enumerate(tweets):
        tokens = gensim.utils.simple_preprocess(tweet)
        if is_validation:
            output.append(tokens)
        else:
            # For training data, add tags
            output.append(gensim.models.doc2vec.TaggedDocument(tokens, [index]))
    return output


def create_and_save_doc2vec_model(
        model_filepath: str,
        corpus: List[gensim.models.doc2vec.TaggedDocument],
        features_per_tweet: int,
        epochs: int,
) -> Doc2Vec:
    """
    Creates a word2vec embedding for the given tweets and save it to the provided path.
    :param corpus: A array of sentences. The word2vec embedding will create embeddings for the words in the sentences.
    :param features_per_tweet: The count of feature to compute for each tweet.
    :param epochs: The number of epoch to train the doc2vec model for.
    :param model_filepath: The path of the file in which the model will be stored.
    :return: The trained model which is also saved to a file.
    """
    logger.info('Training doc2vec embedding model.')
    model = Doc2Vec(vector_size=features_per_tweet, min_count=2, epochs=epochs)
    model.build_vocab(corpus)  # Use all the text here? Or just the training set?
    model.train(corpus, total_examples=model.corpus_count, epochs=model.epochs)
    logger.info('Doc2vec training finished.')
    model.save(model_filepath)
    logger.info(f'Doc2vec model saved to {model_filepath}')
    return model


def load_doc2vec_model(model_filepath: str) -> Doc2Vec:
    """
    Returns an instantiated doc2vec model from its file serialization.
    :param model_filepath: The path to the serialized model.
    :return: A fully instantiated model.
    """
    return Doc2Vec.load(model_filepath)


def get_or_create_doc2vec_model(
        doc2vec_model_filepath: str,
        corpus: List[gensim.models.doc2vec.TaggedDocument],
        features_per_tweet: int,
        epochs: int,
) -> Doc2Vec:
    """
    Either returns the instantiation of the serialization of an existing doc2vec model at the given path or create a
    new doc2vec model from the provided data.
    :param doc2vec_model_filepath: The path where the doc2vec model was stored if it already exist or where it will
    be stored.
    :param corpus: The dataset to train the model on if the model does not already exist.
    :param features_per_tweet: The count of feature to compute for each tweet.
    :param epochs: The number of epoch to train the doc2vec model for.
    :return: A trained doc2vec model.
    """
    if not os.path.exists(doc2vec_model_filepath):
        logger.info('Creating a new doc2vec embedding model.')
        return create_and_save_doc2vec_model(
            doc2vec_model_filepath,
            corpus,
            features_per_tweet=features_per_tweet,
            epochs=epochs)
    else:
        logger.info('Loading doc2vec embedding model from file.')
        return Doc2Vec.load(doc2vec_model_filepath)

import logging

import numpy as np
import os

from gensim.models import Word2Vec

from sentiment_classification.data import SplitTweets
from sentiment_classification.embeddings import infer_fixed_length_tweet_embedding

logger = logging.getLogger(__name__)


def create_and_save_word2vec_embedding(
        model_filepath: str,
        split_tweets: SplitTweets,
        vector_size: int = 20,
        window: int = 4,
        min_count: int = 1,
        workers: int = 4,
):
    """
    Creates a word2vec embedding for the given tweets and save it to the provided path.
    :param split_tweets: A array of sentences. The word2vec embedding will create embeddings for the words in the sentences.
    :param vector_size: The size of the output vector characterizing each word in the vocabulary.
    :param window: The number of words around a given word to consider as being part of the context of the word.
    :param min_count: The minimum number of occurrences a word must have to not be discarded from the dictionary.
    :param workers: On how many workers the training of the model should be parallelized.
    :param model_filepath: The path of the file in which the model will be stored.
    :return: The trained model which is also saved to a file.
    """
    logger.info('Training word2vec embedding model.')
    word2vec_model = Word2Vec(
        sentences=split_tweets,
        vector_size=vector_size,
        window=window,
        min_count=min_count,
        workers=workers)
    word2vec_model.save(model_filepath)
    logger.info('Word2vec training finished.')
    return word2vec_model


def load_word2vec_model(model_filepath: str) -> Word2Vec:
    """
    Returns an instantiated word2vec model from its file serialization.
    :param model_filepath: The path to the serialized model.
    :return: A fully instantiated model.
    """
    return Word2Vec.load(model_filepath)


def get_or_create_word2vec_model(
        word2vec_model_filepath: str,
        split_tweets: SplitTweets,
        vector_size=20
) -> Word2Vec:
    """
    Either returns the instantiation of the serialization of an existing word2vec model at the given path or create a
    new word2vec model from the provided data.
    :param word2vec_model_filepath: The path where the word2vec model was stored if it already exist or where it will
    be stored.
    :param split_tweets: The dataset to train the model on if the model does not already exist.
    :param vector_size: The size of the vector that the model should learn for each word.
    :return: A trained word2vec model
    """
    if not os.path.exists(word2vec_model_filepath):
        logger.info('Creating a new word2vec embedding model.')
        return create_and_save_word2vec_embedding(
            word2vec_model_filepath,
            split_tweets,
            vector_size=vector_size)
    else:
        logger.info('Loading word2vec embedding model from file.')
        return Word2Vec.load(word2vec_model_filepath)


def embed_tweets_with_word2vec(
        word2vec_model: Word2Vec,
        tweets: SplitTweets,
        features_per_tweet: int,
) -> np.array:
    """
    Computes the word2vec embedding of the provided tweets.
    
    The word2vec embedding of each word will be computed.
    These embedding will be concatenated in a single array.
    Then the array will be either clipped to a length equal to the number of feature per tweet or zero-padded to the
    same length.
    :param word2vec_model: The word2vec model to use to embed tweets.
    :param tweets: The tweets to embedded.
    :param features_per_tweet: The number of features to generate for each tweet.
    :return:
    """
    logger.info('Embedding tweets with word2vec.')
    return np.array([
        infer_fixed_length_tweet_embedding(
            word2vec_model,
            tweet,
            features_per_tweet,
            lambda model, word: model.wv[word]
        ) for tweet in tweets])

import logging
from typing import Dict

import numpy as np

from sentiment_classification.data import SplitTweets
from sentiment_classification.embeddings import infer_fixed_length_tweet_embedding

logger = logging.getLogger(__name__)


def load_glove_embedding(embedding_filepath: str) -> Dict[str, np.array]:
    """
    Loads and return a GloVe embedding from a file on disk.
    :param embedding_filepath: The path to the serialized GloVe embedding.
    The serialization should be formatted in the following way: each word is encoded on a line.
    First the word is given, then a list of numbers representing the embedding, separated by commas.
    :return: A dictionary mapping each word to its GloVe embedding.
    """
    logger.info(f'Loading GloVe embedding from {embedding_filepath}.')
    glove_embeddings = {}
    with open(embedding_filepath, 'r') as f:
        for line in f:
            values = line.split()
            word = values[0]
            vector = np.asarray(values[1:], "float32")
            glove_embeddings[word] = vector
    logger.info(f'Loaded GloVe embeddings for {len(glove_embeddings)} words.')
    return glove_embeddings


def embed_tweets_with_glove(
        glove_model: Dict[str, np.array],
        split_tweets: SplitTweets,
        features_per_tweet: int,
) -> np.array:
    """
    Computes the glove embedding of the provided tweets.

    The glove embedding of each word will be computed.
    These embedding will be concatenated in a single array.
    Then the array will be either clipped to a length equal to the number of feature per tweet or zero-padded to the
    same length.
    :param glove_model: The glove model to use to embed tweets.
    :param split_tweets: The tweets to embedded.
    :param features_per_tweet: The number of features to generate for each tweet.
    :return:
    """
    logger.info('Embedding tweets with GloVe.')
    return np.array([
        infer_fixed_length_tweet_embedding(
            glove_model,
            tweet,
            features_per_tweet,
            lambda model, word: model[word] if word in model else None
        ) for tweet in split_tweets])

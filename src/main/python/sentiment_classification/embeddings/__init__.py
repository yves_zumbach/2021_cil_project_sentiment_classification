import logging
from typing import Callable, List, Any, Optional

import numpy as np

from sentiment_classification.data import SplitTweet, Word


logger = logging.getLogger(__name__)


def infer_fixed_length_tweet_embedding(
        model,
        split_tweet: SplitTweet,
        features_count: int,
        embedding_from_model_lambda: Callable[[Any, Word], Optional[List[float]]],
) -> np.array:
    """
    Embed each word in the sentence then produce a fixed-length embedding for each sentence.
    If the concatenation of the embeddings of all the words in the tweet produces more features than permitted,
    they will be clipped to the maximum allowed count.
    If the concatenation of the embeddings of all the words in the tweet produces less features than required,
    then the features will be zero-padded.
    :param model: The word embedding model to use.
    :param split_tweet: An list of words that form a sentence.
    :param features_count: The number of feature to produce for each tweet.
    :param embedding_from_model_lambda: A lambda that takes a model and a word as input and produces the embedding of
    the word.
    :return: A numpy array of size equal to the requested feature count containing the features of the given tweet.
    """
    tweet_embedding = []
    for word in split_tweet:
        word_embedding = embedding_from_model_lambda(model, word)
        if word_embedding is not None:
            tweet_embedding.append(word_embedding)

    if not tweet_embedding:
        logger.warning(
            f'No word from tweet "{split_tweet}" was found in the GloVe embeddings... Using zero-filled feature vector.'
        )
        tweet_embedding = [[0]]

    # Flatten the embedding from a list of list of float to a list of float.
    tweet_embedding = np.concatenate(tweet_embedding)

    # Clip the output array to the maximum number of features if there are more features available.
    if len(tweet_embedding) >= features_count:
        return tweet_embedding[:features_count]
    # Pad the output with zero features if there are not enough features available.
    else:
        return np.hstack((np.zeros(features_count - len(tweet_embedding)), tweet_embedding))

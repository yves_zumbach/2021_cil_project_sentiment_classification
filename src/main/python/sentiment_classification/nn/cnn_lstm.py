import torch


class CNNLSTM(torch.nn.Module):
    """
    A simple 1DCNN-LSTM neural network.
    """

    def __init__(self, input_dim, time_step=50, hidden_dim=100, dropout=0.25, conv_kernel_size=2, max_pool_kernel_size=3):
        super(CNNLSTM, self).__init__()
        self.input_dim = input_dim
        self.time_step = time_step
        self.hidden_dim = hidden_dim
        self.conv_kernel_size = 2
        self.max_pool_kernel_size = 3

        self.conv1 = torch.nn.Conv1d(in_channels=input_dim, out_channels=hidden_dim, kernel_size=conv_kernel_size, padding_mode='zeros', padding=0)
        self.max_pool1 = torch.nn.MaxPool1d(kernel_size=max_pool_kernel_size, padding=0)

        self.lstm_input_dim = (self.time_step - (conv_kernel_size - 1)) // self.max_pool_kernel_size
        self.layers = []
        self.lstm = torch.nn.LSTM(input_size=self.lstm_input_dim, hidden_size=self.hidden_dim)

        self.layers.append(torch.nn.Linear(self.hidden_dim, 1))
        self.layers.append(torch.nn.Dropout(dropout))
        self.layers.append(torch.nn.Sigmoid())
        self.layers = torch.nn.ModuleList(self.layers)

    def forward(self, x):

        x = self.conv1(x)
        x = self.max_pool1(x)

        hidden = None
        out, hidden = self.lstm(x, hidden)
        x = out[:, -1, :]
        for layer in self.layers:
            x = layer(x)

        return x
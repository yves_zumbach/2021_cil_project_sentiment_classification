import logging

import torch
from torch import Tensor
from torch.nn import Module
from torch.optim import Optimizer
from torch.utils.data import TensorDataset, DataLoader
from tqdm import tqdm

from sentiment_classification.metrics import compute_binary_accuracy

logger = logging.getLogger(__name__)


def create_dataloader(x: Tensor, y: Tensor, mini_batch_size):
    """
    Returns a data loader for the given data with the mini-batch size provided as argument.
    :param x: The data.
    :param y: The labels of the data.
    :param mini_batch_size: The size of the mini-batch that the data loader should produce.
    :return: A fully instantiated data loader.
    """
    dataset = TensorDataset(x, y)
    dataloader = DataLoader(
        dataset=dataset,
        batch_size=mini_batch_size,
        shuffle=True
    )
    return dataloader


def train(
        model_filepath: str,
        model: Module,
        epoch: int,
        dataloader_train: DataLoader,
        dataloader_validation: DataLoader,
        optimizer: Optimizer,
        criterion
) -> None:
    """
    Trains a neural network using mini-batches from the provided data loader.
    The trained model is saved to a file in the build folder at the end of the training.
    :param model_filepath: The path of the file in which the model should be saved.
    :param model: A PyTorch network to train.
    :param epoch: The number of epoch to train the model for. An epoch is a single pass over the full dataset.
    :param dataloader_train: The data loader from which to take the training data.
    :param dataloader_validation: The data loader from which to obtain the validation data
    :param optimizer: The optimizer that will make the model learn.
    :param criterion: The loss function to use to optimize the model.
    """
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = model.to(device)
    best_val_acc = 0

    for i in tqdm(range(epoch), position=0):
        model.train()
        logger.info('Epoch: {} starts!'.format(i + 1))
        for step, (batch_x, batch_y) in enumerate(dataloader_train):
            batch_x = batch_x.to(device)
            batch_y = batch_y.to(device)
            optimizer.zero_grad()
            prediction_y = model(batch_x.to(device))
            loss = criterion(prediction_y, batch_y.to(device))
            if step % 50 == 0:
                logger.info('Step: {}'.format(step + 1))
                logger.info('Batch loss = {}'.format(loss.item()))
            loss.backward()
            optimizer.step()
            del batch_x
            del batch_y
            del prediction_y

        # evaluate the model on the validation dataset after each epoch
        cumulative_validation_accuracy = 0
        cumulative_validation_size = 0

        cumulative_training_accuracy = 0
        cumulative_training_size = 0

        model.eval()
        with torch.no_grad():

            for j, (batch_x, batch_y) in enumerate(dataloader_validation):
                batch_x = batch_x.to(device)
                batch_y = batch_y.to(device)
                prediction_y = model(batch_x)
                cumulative_validation_accuracy += len(batch_y) * compute_binary_accuracy(prediction_y, batch_y)
                cumulative_validation_size += len(batch_y)
                del batch_x
                del batch_y
                del prediction_y

            for k, (batch_x, batch_y) in enumerate(dataloader_train):
                batch_x = batch_x.to(device)
                batch_y = batch_y.to(device)
                prediction_y = model(batch_x)
                cumulative_training_accuracy += len(batch_y) * compute_binary_accuracy(prediction_y, batch_y)
                cumulative_training_size += len(batch_y)

                del batch_x
                del batch_y
                del prediction_y

            training_accuracy = cumulative_training_accuracy / cumulative_training_size
            validation_accuracy = cumulative_validation_accuracy / cumulative_validation_size

            logger.info('Training accuracy (whole training dataset): {}'.format(training_accuracy))
            logger.info('Validation accuracy: {}'.format(validation_accuracy))
            if validation_accuracy > best_val_acc and training_accuracy - validation_accuracy <= 0.001:
                best_val_acc = validation_accuracy
                torch.save(model, '{}_{}{}'.format(model_filepath[:-4], 'best', '.pth'))

    logger.info('Saving model...')
    torch.save(model, '{}_{}{}'.format(model_filepath[:-4], 'final', '.pth'))
    logger.info('Training finished!')

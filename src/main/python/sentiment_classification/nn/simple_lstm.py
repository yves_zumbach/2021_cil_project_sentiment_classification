import torch


class SimpleLSTM(torch.nn.Module):
    """
    A simple LSTM network without any convolution operation.
    """

    def __init__(self, input_dim, hidden_dim=100, dropout=0.25):
        super(SimpleLSTM, self).__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.layers = []
        self.lstm = torch.nn.LSTM(input_size=self.input_dim, hidden_size=self.hidden_dim)

        self.layers.append(torch.nn.Linear(self.hidden_dim, 1))
        self.layers.append(torch.nn.Dropout(dropout))
        self.layers.append(torch.nn.Sigmoid())
        self.layers = torch.nn.ModuleList(self.layers)

    def forward(self, x):

        hidden = None
        out, hidden = self.lstm(x, hidden)
        x = out[:, -1, :]
        for layer in self.layers:
            x = layer(x)

        return x
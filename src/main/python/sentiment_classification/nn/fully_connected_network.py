import torch


class FullyConnectedNetwork(torch.nn.Module):
    """
    A fully connected neural network.
    """

    def __init__(self, num_layers=2, input_dim=5000, hidden_dim=2000, dropout=0.25):
        super(FullyConnectedNetwork, self).__init__()
        self.layers = []
        self.layers.append(torch.nn.Linear(input_dim, hidden_dim))
        self.layers.append(torch.nn.ReLU())

        for i in range(1, num_layers):
            self.layers.append(torch.nn.Linear(hidden_dim, hidden_dim))
            self.layers.append(torch.nn.Dropout(dropout))
            self.layers.append(torch.nn.ReLU())
        
        self.layers.append(torch.nn.Linear(hidden_dim, 1))
        self.layers.append(torch.nn.Sigmoid())
        self.layers = torch.nn.ModuleList(self.layers)
    
    def forward(self, x):
        # x1 = None
        # for i in range(len(self.layers)):
        #     layer = self.layers[i]
        #     if i == 1:
        #         x1 = layer(x)
        #         x = layer(x)
        #     elif i == len(self.layers) - 2:
        #         x = layer(torch.cat((x, x1), dim=1))
        #     else:
        #         x = layer(x)
        for i in range(len(self.layers)):
            x = self.layers[i](x)
        return x

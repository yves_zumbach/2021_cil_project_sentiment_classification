from typing import Union

import numpy as np
import torch
from torch import Tensor


def compute_binary_accuracy(predictions_y: Union[Tensor, np.ndarray], y: Union[Tensor, np.ndarray]):
    """
    Compute accuracy of predictions based on sigmoid predictions and ground-truth.
    The sigmoid predictions, i.e. predictions in the range [0, 1] will be converted to 0 or 1 value and compared to
    their ground-truth.
    :param predictions_y: The predictions given in a [0, 1] range.
    :param y: The ground truth prediction, i.e. 0 or 1 values.
    :return: The ratio of correct predictions over the total number of predictions.
    """
    if isinstance(predictions_y, Tensor) and isinstance(y, Tensor):
        assert predictions_y.size()[0] == y.size()[0], 'Predictions and labels do not have the same length'
        predictions_y[torch.where(predictions_y < 0.5)] = 0
        predictions_y[torch.where(predictions_y >= 0.5)] = 1
        return torch.where(predictions_y == y)[0].size()[0] / predictions_y.size()[0]

    elif isinstance(predictions_y, np.ndarray) and isinstance(y, np.ndarray):
        assert len(predictions_y) == len(y), 'Predictions and labels do not have the same length'

        predictions_y[predictions_y < 0.5] = 0
        predictions_y[predictions_y >= 0.5] = 1
        return sum(predictions_y == y) / len(predictions_y)

    else:
        raise Exception(f'Wrong types received: predictions_y and y must both be either tensorflow.Tensor or '
                        f'numpy.array. predictions_y: {type(predictions_y)}, y: {type(y)}.')